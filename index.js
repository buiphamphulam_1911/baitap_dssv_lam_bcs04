const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";

//  chức năng thêm sinh viên
var dssv = [];
// lấy thông tin từ localStorage

var dssvJson = localStorage.getItem(DSSV_LOCALSTORAGE);
if (dssvJson != null) {
  dssv = JSON.parse(dssvJson);
  //  array khi convert thành json sẽ mất function, ta sẽ map lại
  for (var index = 0; index < dssv.length; index++) {
    var sv = dssv[index];
    dssv[index] = new SinhVien(
      sv.ma,
      sv.ten,
      sv.matKhau,
      sv.email,
      sv.toan,
      sv.ly,
      sv.hoa
    );
  }

  renderDSSV(dssv);
}
function themSV() {
  var newSv = layThongTinTuForm();
  // console.log("newSv: ", newSv);

  var isValid =
    validation.kiemTraRong(
      newSv.ma,
      "spanMaSV",
      "Mã sinh viên không được rỗng"
    ) &&
    validation.kiemTraDoDai(
      newSv.ma,
      "spanMaSV",
      "Mã sinh viên phải gồm 4 kí tự",
      1,
      4
    );
  isValid =
    isValid &
    validation.kiemTraRong(
      newSv.ten,
      "spanTenSV",
      "Tên sinh viên không được rỗng"
    );
    isValid= isValid & 
  
    validation.kiemTraEmail(newSv.email,"spanEmailSV","Email phải có @,.com , ...");

    isValid= isValid & 
    validation.kiemTraRong(newSv.matKhau,"spanMatKhau","Mật khẩu không được để rổng.")&&
    validation.kiemTraMatKhau(
      newSv.matKhau,
      "spanMatKhau",
      "Mật khẩu phải chứa ít nhất 1 số, 1 chữ hoa, 1 chữ thường và độ dài ít nhất là 8 ký tự.",
    );
    isValid =isValid &
    validation.kiemTraRong(
      newSv.toan,
      "spanToan", 
      "Điểm Toán không được rỗng"
    ) &&
    validation.kiemtraSo(
      newSv.toan,
      "spanToan",
      "Chỉ được nhập số ",
    );
    isValid =isValid &
    validation.kiemTraRong(
      newSv.ly,
      "spanLy",
      "Chỉ được nhập số ",
    ) &&
    validation.kiemtraSo(
      newSv.ly,
      "spanLy",
      "Chỉ được nhập số ",
    );
    isValid =isValid &
    validation.kiemTraRong(
      newSv.hoa,
      "spanHoa",
      "Điểm Hóa không được rỗng"
    ) &&
    validation.kiemtraSo(
      newSv.hoa,
      "spanHoa",
      "Chỉ được nhập số ",
    );
  if (isValid) {
    dssv.push(newSv);
    // tạo json
    var dssvJson = JSON.stringify(dssv);
    // console.log("dssvJson: ", dssvJson);
    // lưu json vào localStorage
    localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
    renderDSSV(dssv);
  }
}

function xoaSinhVien(id) {

  // var index = dssv.findIndex(function (sv) {
  //   return sv.ma == id;
  // });
  var index = timKiemViTri(id, dssv);
  // tìm thấy vị trí
  if (index != -1) {
    dssv.splice(index, 1);
    renderDSSV(dssv);
  }
}

function suaSinhVien(id) {
  var index = timKiemViTri(id, dssv);
  console.log("index: ", index);
  if (index != -1) {
    var sv = dssv[index];
    showThongTinLenForm(sv);
  }
}
function capNhatSV(){
  var updateSV = layThongTinTuForm();
  var index = timKiemViTri(updateSV.ma,dssv);
  dssv[index] = updateSV;
  var dssvJson = JSON.stringify(dssv);
  localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
  renderDSSV(dssv);
}
function timkiemSV (){
  var tenSV  = document.getElementById("txtSearch").value;
  var contentHTML = "";
  for (var i = 0; i < dssv.length; i++) {
    var sv = dssv[i];
    if(sv.ten == tenSV){
    var trContent = ` <tr>
    <td>${sv.ma}</td>
    <td>${sv.ten}</td>
    <td>${sv.email}</td>
    <td>${sv.tinhDTB()}</td>
    <td>
    <button onclick="xoaSinhVien('${
      sv.ma
    }')" class="btn btn-danger">Xoá</button>
    <button 
    onclick="suaSinhVien('${sv.ma}')"
    class="btn btn-warning">Sửa</button>
    </td>
    </tr>`;
    contentHTML += trContent;
    document.getElementById("tbodySinhVien").innerHTML = contentHTML;
    }
  }
}
function resetSV(){""
  document.getElementById("txtMaSV").value = "";
  document.getElementById("txtTenSV").value = "";
  document.getElementById("txtEmail").value = "";
  document.getElementById("txtPass").value = "";
  document.getElementById("txtDiemToan").value ="";
  document.getElementById("txtDiemLy").value = "";
  document.getElementById("txtDiemHoa").value = "";
}


